var fs = require('fs')
var path = require('path')
var parseUrl = require('parseurl')

function makeExpressStaticCaseSensitive (express, options) {
  options = Object.assign({
    cacheTimeout: 5000
  }, options)

  var dirCache = {}

  function getFilesInDirCached (dirpath, callback) {
    let cacheEntry = dirCache[dirpath]

    // Sometimes we receive multiple requests for this dirpath while we are still waiting for readdir() to respond
    // So if there is an ongoing readdir() (indicated by the presence of 'pendingCallbacks' in the cacheEntry), then we will append the callback to that array
    // This could perhaps be written more intuitively using promises

    if (cacheEntry && Date.now() < cacheEntry.when + options.cacheTimeout) {
      if (cacheEntry.pendingCallbacks) {
        cacheEntry.pendingCallbacks.push(callback)
      } else {
        callback(null, cacheEntry.mapOfFiles)
      }
    } else {
      // console.log(`! Creating cache entry for ${dirpath}`)
      cacheEntry = {
        when: Date.now(),
        pendingCallbacks: [callback]
      }
      dirCache[dirpath] = cacheEntry

      fs.readdir(dirpath, function (err, files) {
        if (err) return callback(err)

        // Lookup from a map is more efficient than from an array
        var mapOfFiles = {}
        files.forEach(file => {
          mapOfFiles[file] = true
        })

        cacheEntry.mapOfFiles = mapOfFiles
        // console.log(`. done creating cache entry for ${dirpath}`)

        // Now we can call the function or functions that are waiting for this result
        cacheEntry.pendingCallbacks.forEach(function (cb) {
          cb(null, cacheEntry.mapOfFiles)
        })

        // Subsequent requests for this dirpath can callback immediately
        delete cacheEntry.pendingCallbacks
      })
    }
  }

  // Returns true if the given path exists below dirpath (case-sensitive)
  // If not, returns the part of the path that failed, as a string.
  function checkPathBits (dirpath, pathBits, callback) {
    if (pathBits.length === 0) {
      return callback(null, true)
    }

    var nextNode = pathBits.shift()
    var nextPath = path.join(dirpath, nextNode)

    getFilesInDirCached(dirpath, function (err, mapOfFiles) {
      if (err) return callback(err)

      var nodeIsThere = mapOfFiles[nextNode]
      if (!nodeIsThere) {
        callback(null, nextNode)
      } else {
        checkPathBits(nextPath, pathBits, callback)
      }
    })
  }

  var oldStatic = express.static
  var newStatic = function (root, options) {
    var opts = Object.create(options || null)

    var fallthrough = opts.fallthrough !== false

    var originalHandler = oldStatic(root, options)

    var wrappedHandler = function (req, res, next) {
      // serveStatic's normal behaviour for GET or HEAD requests (copy-pasted)
      if (req.method !== 'GET' && req.method !== 'HEAD') {
        if (fallthrough) {
          return next()
        }

        // method not allowed
        res.statusCode = 405
        res.setHeader('Allow', 'GET, HEAD')
        res.setHeader('Content-Length', '0')
        res.end()
        return
      }

      // fs.createReadStream() seems happy to serve up requests containing '%20' if there is a file containing ' ', at least on my Mac it does.
      // But since we try to lookup the file from a list, I do decodeURIComponent() here to get the correct value.
      var requestedPath = decodeURIComponent(parseUrl(req).pathname).replace(/^\//, '')
      var pathBits = requestedPath ? requestedPath.split('/') : []

      // console.log("> Received request for:", requestedPath)

      checkPathBits(root, pathBits, function (err, result) {
        if (err) return next(err)

        if (result === true) {
          // The file exists, with matching case, so let express-static serve it as normal
          originalHandler(req, res, next)
        } else {
          // @todo We might want to do something special if the file exists but with non-matching case

          // express-static's usual behaviour for missing files is to continue to the next express handler if fallthrough is set
          if (fallthrough) {
            return next()
          }

          // Or if fallthrough is not set, it tries to read the file and fails, resulting in something like this
          // res.status(404).end()
          next({ statusCode: 404, message: 'File not found' })
        }
      })
    }

    return wrappedHandler
  }
  express.static = newStatic
}

module.exports = makeExpressStaticCaseSensitive
