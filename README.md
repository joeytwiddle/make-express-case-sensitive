# make-express-case-sensitive

## What is it good for?

If you and your team develop on Windows or MacOS, you can get away with misnaming your filenames and assets with the wrong case, because the OS will still load a file with different case off the disk.  Even locally run tests will pass.  But when you deploy your files to a testing or production server which runs Linux, then the app will no longer be able to find the misnamed files!

This module will force the express static middleware to care about misnamed files, even if you are using a case-insensitive filesystem.  This will help you to catch errors before they leave your machine.

## How to use it?

We only enable it in development mode:

```js
var express = require('express');

var inDevelopment = (process.env.NODE_ENV || 'local') === 'local';

if (inDevelopment) {
    require('make-express-case-sensitive')(express);
}

// Go ahead and use express.static() as you normally would
// ...
```

Note that this will actually mutate the function at `express.static` so this will affect all references to `express.static` in other modules too, provided they execute after this code has run.

## Notes

To speed up situations where you have a lot of assets/files to load, this package keeps a cache of the status of each folder it reads, for 5 seconds.
